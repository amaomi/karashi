/* scanner.h - first stage of the REPL, process user input and create tokens */

/* Copyright (c) 2022 Andrey Sikorin.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KARASHI_SRC_READ_H_
#define KARASHI_SRC_READ_H_

#include <stddef.h>

// First state of data after user raw input
struct tokens {
  enum tokens_type {
    VALID, // Succeeded state of tokens struct
    INVALID, // Failure during tokenization process
  } type;
  char** data; // Array of strings, each represent individual token
  size_t amount; // Size of tokens array
};

// Process user input and convert it into the struct tokens
struct tokens input(void);

// Free array of strings in struct tokens
void free_tokens(struct tokens tokens);

#endif //KARASHI_SRC_READ_H_
