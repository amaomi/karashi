/* utility.h - error handling functions */

/* Copyright (c) 2022 Andrey Sikorin.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KARASHI_SRC_UTILITY_H_
#define KARASHI_SRC_UTILITY_H_

#include <stdbool.h>

// Terminals red output option
#define BOLD_RED "\033[1;31m"
// Terminals standard output option
#define RESET    "\033[0m"

// If ptr is NULL, print error message end return true,
// otherwise just return false
bool check_alloc(const void* ptr, const char msg[]);

// If ptr is NULL, print error message and exit program
void assert_alloc(const void* ptr, const char msg[]);

// Wrapper for getenv function. If env_var is not in environment list,
// print error message and exit program
char* check_getenv(const char* env_var);

// Print colored errno message to stdout
void print_errno(void);

#endif //KARASHI_SRC_UTILITY_H_
