/* built-in.h - contents all built-in kara commands and checker for them */

/* Copyright (c) 2022 Andrey Sikorin.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KARASHI_SRC_BUILT_IN_H_
#define KARASHI_SRC_BUILT_IN_H_

#include <stdbool.h>

// Kara shell built-in commands
#define CD "cd"
#define EXIT "exit"

// Determine if string is built-in kara command
bool is_in_table(const char string[]);

#endif //KARASHI_SRC_BUILT_IN_H_
