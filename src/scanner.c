/* scanner.c - get user input via readline library and split it into tokens */

/* Copyright (c) 2022 Andrey Sikorin.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "scanner.h"

#include <stdio.h>
#include <stdlib.h>

// GNU readline library headers
#include <readline.h>
#include <history.h>

#include "prompt.h"
#include "utility.h"

// Determine if user input content only space characters
static bool is_skip(const char* string);

// Split string into the struct tokens
static struct tokens tokenize(char* string);

// Used in exceptional cases in the tokenize function
static void free_resources(char* string, struct tokens tokens);

static const struct tokens invalid_tokens = {INVALID, NULL, 0};

struct tokens input(void) {
  char* string = NULL;

  while (true) {
    char* prompt = get_prompt();
    string = readline(prompt);
    free(prompt);

    if (!is_skip(string)) {
      break;
    }
    rl_free(string);
  }
  add_history(string);

  return tokenize(string);
}

void free_tokens(struct tokens tokens) {
  if (!tokens.data || tokens.type == INVALID) {
    return;
  }
  for (size_t i = 0; i < tokens.amount; ++i) {
    free(tokens.data[i]);
  }
  free(tokens.data);
}

////////////////////////////////////////////////////////////////////////////////
// Static functions ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

static bool is_skip(const char* string) {
  // EOF check
  if (!string) {
    putchar('\n');
    exit(EXIT_SUCCESS);
  }
  while (isspace(*string)) {
    ++string;
  }
  return *string == '\0';
}

static struct tokens tokenize(char* string) {
  struct tokens tokens = {VALID, NULL, 0};

  char* tok = strtok(string, " \t\n");

  while (tok) {
    tokens.data = realloc(tokens.data, (tokens.amount + 1) * sizeof(char*));
    if (!check_alloc(tokens.data, "tokens")) {
      free_resources(string, tokens);
      return invalid_tokens;
    }

    size_t extra_len = 0;
    if (tok[0] == '~') {
      extra_len = strlen(check_getenv("HOME"));
    } else if (tok[0] == '$' && getenv(tok + 1)) {
      extra_len = strlen(getenv(tok + 1));
    }
    tokens.data[tokens.amount] = malloc(strlen(tok) + extra_len + 1);
    if (!check_alloc(tokens.data, "token")) {
      free_resources(string, tokens);
      return invalid_tokens;
    }

    if (tok[0] == '~') {
      strcpy(tokens.data[tokens.amount], getenv("HOME"));
      strcat(tokens.data[tokens.amount], tok + 1);
    } else if (tok[0] == '$' && extra_len) {
      strcpy(tokens.data[tokens.amount], getenv(tok + 1));
    } else {
      strcpy(tokens.data[tokens.amount], tok);
    }
    tokens.amount++;

    tok = strtok(NULL, " \t\n");
  }

  rl_free(string);
  return tokens;
}

static void free_resources(char* string, struct tokens tokens) {
  rl_free(string);
  free_tokens(tokens);
}
