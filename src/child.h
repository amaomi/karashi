/* child.h - interface for communication with child processes */

/* Copyright (c) 2022 Andrey Sikorin.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KARASHI_SRC_CHILD_H_
#define KARASHI_SRC_CHILD_H_

#include <stddef.h>
#include <semaphore.h>

// Set unique name for kara semaphore
void set_sem_name(void);

// Send specific signal to all current child processes
void send_signal_to_child(int sig);

// Force termination of all child processes if any and close semaphore
void clear_child(void);

// Array of child process id which is alive during pipes handling
extern pid_t  child_pid[];
// Child pids array size
extern size_t child_amount;

// Used for child synchronisation during pipes handling
extern sem_t* sem;
// Unique semaphore name
extern char sem_name[];

#endif //KARASHI_SRC_CHILD_H_
