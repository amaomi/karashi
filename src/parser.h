/* parser.h - second stage of the REPL, build ast based on tokens */

/* Copyright (c) 2022 Andrey Sikorin.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KARASHI_SRC_PARSER_H_
#define KARASHI_SRC_PARSER_H_

#include "scanner.h"

// Amount of standard streams (stdin, stdout, stderr)
#define TOTAL_STREAMS 3

// Single command to execute
struct command {
  enum command_type {
    BUILT_IN, // Functionality that is not stored as separate executable
    EXTERNAL, // Executable program that is stored somewhere on drive
    UNKNOWN, // Represent command with not specified yet type
  } type;
  char* name; // Built-in or external name of the command
  char* redirect[TOTAL_STREAMS]; // Redirection file paths, NULL means stdio
  char** args; // Array of strings which represents external commands args
  size_t args_amount; // Size of arguments array
};

// List of commands, each command is piped to next one
struct abstract_syntax_tree {
  struct command* nodes; // Array of commands
  size_t amount; // Size of commands array
};

// Convert struct tokens into struct abstract_syntax_tree
struct abstract_syntax_tree parse(struct tokens tokens);

// Free array of commands and each command heap-allocated field
void free_ast(struct abstract_syntax_tree ast);

#endif //KARASHI_SRC_PARSER_H_
