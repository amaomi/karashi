/* parser.c - process tokens and allocate array of commands to execute */

/* Copyright (c) 2022 Andrey Sikorin.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "parser.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include "built-in.h"
#include "utility.h"

// Add node with specific name to ast
static struct command* add_node(struct abstract_syntax_tree* ast,
                                const char* node_name);

// Allocate new empty node for ast
static bool extend_ast(struct abstract_syntax_tree* ast);

// Allocate and set new name string for node
static bool add_name(struct command* node, const char* name);

// Allocate and set next argument for node
static bool add_arg(struct command* node, const char* arg);

// Allocate and set redirection file path for specific standard process stream
static bool add_redirection(struct command* node,
                            const char* file_path,
                            int stream_index);

// Verify that built-in commands is not used in pipe sequence
static bool is_allowed_sequence(struct abstract_syntax_tree ast);

// Used in exceptional cases in the parse function
static void free_resources(struct tokens tokens,
                           struct abstract_syntax_tree ast);

static const struct abstract_syntax_tree empty_ast = {NULL, 0};

struct abstract_syntax_tree parse(struct tokens tokens) {
  if (tokens.type != VALID) {
    return empty_ast;
  }

  struct abstract_syntax_tree ast = {NULL, 0};

  struct command* node = add_node(&ast, tokens.data[0]);
  if (!node) {
    free_resources(tokens, ast);
    return empty_ast;
  }

  size_t i = 1;
  while (i < tokens.amount) {
    int std_stream = -1;
    if (!strcmp(tokens.data[i], "<")) {
      std_stream = STDIN_FILENO;
    } else if (!strcmp(tokens.data[i], ">")) {
      std_stream = STDOUT_FILENO;
    } else if (!strcmp(tokens.data[i], "2>")) {
      std_stream = STDERR_FILENO;
    }
    if (std_stream != -1) {
      if (++i == tokens.amount ||
          !add_redirection(node, tokens.data[i], std_stream)) {
        free_resources(tokens, ast);
        return empty_ast;
      }

    } else if (!strcmp(tokens.data[i], "|")) {
      // Add NULL as last argument in previous node
      add_arg(node, "");

      if (++i == tokens.amount ||
          !(node = add_node(&ast, tokens.data[i]))) {
        free_resources(tokens, ast);
        return empty_ast;
      }

    } else if (!add_arg(node, tokens.data[i])) {
      free_resources(tokens, ast);
      return empty_ast;
    }
    ++i;
  }
  // Add NULL as last argument in last node
  add_arg(node, "");

  if (!is_allowed_sequence(ast)) {
    printf(BOLD_RED);
    printf("kara: built-in commands are not allowed to use with pipes\n");
    printf(RESET);
    free_resources(tokens, ast);
    return empty_ast;
  }

  free_tokens(tokens);
  return ast;
}

void free_ast(struct abstract_syntax_tree ast) {
  if (!ast.nodes) {
    return;
  }

  for (size_t i = 0; i < ast.amount; ++i) {
    free(ast.nodes[i].name);

    for (size_t j = 0; j < ast.nodes[i].args_amount; ++j) {
      free(ast.nodes[i].args[j]);
    }
    free(ast.nodes[i].args);

    for (size_t j = 0; j < TOTAL_STREAMS; ++j) {
      free(ast.nodes[i].redirect[j]);
    }
  }

  free(ast.nodes);
}

////////////////////////////////////////////////////////////////////////////////
// Static functions ////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

static struct command* add_node(struct abstract_syntax_tree* ast,
                                const char* node_name) {
  if (!extend_ast(ast)) {
    return NULL;
  }

  struct command* node = &ast->nodes[ast->amount - 1];
  if (!add_name(node, node_name) ||
      !add_arg(node, node_name)) {
    return NULL;
  }

  node->type = is_in_table(node->name) ? BUILT_IN : EXTERNAL;

  return node;
}

static bool extend_ast(struct abstract_syntax_tree* ast) {
  if (!ast) {
    return false;
  }

  ast->nodes = realloc(ast->nodes, (ast->amount + 1) * sizeof(struct command));
  if (!check_alloc(ast->nodes, "node")) {
    return false;
  }

  struct command* node = &ast->nodes[ast->amount];

  node->type = UNKNOWN;
  node->name = NULL;
  for (size_t i = 0; i < TOTAL_STREAMS; ++i) {
    node->redirect[i] = NULL;
  }
  node->args        = NULL;
  node->args_amount = 0;
  ast->amount++;

  return true;
}

static bool add_name(struct command* node, const char* name) {
  if (!node) {
    return false;
  }

  node->name = malloc(strlen(name) + 1);
  if (!check_alloc(node->name, "node name")) {
    return false;
  }
  strcpy(node->name, name);
  return true;
}

static bool add_arg(struct command* node, const char* arg) {
  node->args = realloc(node->args, (node->args_amount + 1) * sizeof(char*));
  if (!check_alloc(node->args, "node args")) {
    return false;
  }

  if (strlen(arg)) {
    node->args[node->args_amount] = malloc(strlen(arg) + 1);
    if (!check_alloc(node->args[node->args_amount], "node arg")) {
      return false;
    }
    strcpy(node->args[node->args_amount], arg);

  } else {
    node->args[node->args_amount] = NULL;
  }

  node->args_amount++;

  return true;
}

static bool add_redirection(struct command* node,
                            const char* file_path,
                            int stream_index) {
  node->redirect[stream_index] = malloc(strlen(file_path) + 1);
  if (!check_alloc(node->redirect[stream_index], "std stream redirect")) {
    return false;
  }
  strcpy(node->redirect[stream_index], file_path);
  return true;
}

static bool is_allowed_sequence(struct abstract_syntax_tree ast) {
  if (ast.amount > 1 && ast.nodes[0].type == BUILT_IN) {
    return false;
  }
  for (size_t i = 1; i < ast.amount; ++i) {
    if (ast.nodes[i].type == BUILT_IN) {
      return false;
    }
  }
  return true;
}

static void free_resources(struct tokens tokens,
                           struct abstract_syntax_tree ast) {
  free_tokens(tokens);
  free_ast(ast);
}
