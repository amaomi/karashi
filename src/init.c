/* init.c - configure atexit handler and signals redirection to child */

/* Copyright (c) 2022 Andrey Sikorin.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "init.h"

#include <stdlib.h>
#include <signal.h>

#include <unistd.h>

#include "child.h"

// Redirect signals to child processes
static void signal_handler(int sig);

// Helps to prevent recursion calls in signal_handler
static pid_t kara_pid;

void init(void) {
  kara_pid = getpid();
  set_sem_name();

  signal(SIGINT, signal_handler);
  signal(SIGQUIT, signal_handler);
  signal(SIGTSTP, signal_handler);
  signal(SIGSTOP, signal_handler);
  signal(SIGCONT, signal_handler);

  atexit(clear_child);
}

static void signal_handler(int sig) {
  if (getpid() == kara_pid) {
    send_signal_to_child(sig);
  } else {
    signal(sig, SIG_DFL);
    raise(sig);
  }
}
