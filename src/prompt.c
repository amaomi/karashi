/* prompt.c - handle internal logic of what prompt should content */

/* Copyright (c) 2022 Andrey Sikorin.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "prompt.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include "utility.h"

// Change kara hieroglyph color
static void set_prompt_color(void);

// Kara hieroglyph, which is printed instead of default boring '$' symbol
static char kara[] = BOLD_RED " 殻 " RESET;

// Swap $HOME with '~' symbol in the current working directory (cwd) path
static const char TILDE[] = "~";

char* get_prompt(void) {
  char * prompt = NULL;

  char* cwd = getcwd(NULL, 0);
  assert_alloc(cwd, "cwd");

  size_t prompt_length = strlen(cwd) + strlen(kara) + 1;

  char* HOME = check_getenv("HOME");
  bool home_dir = strstr(cwd, HOME) == cwd;
  if (home_dir) {
    prompt_length = prompt_length - strlen(HOME) + strlen(TILDE);
  }

  prompt = malloc(prompt_length);
  assert_alloc(prompt, "prompt");

  if (home_dir) {
    strcpy(prompt, TILDE);
    strcat(prompt, cwd + strlen(HOME));
  } else {
    strcpy(prompt, cwd);
  }

  set_prompt_color();
  strcat(prompt, kara);

  free(cwd);
  return prompt;
}

static void set_prompt_color(void) {
  static const int RED        = 31;
  static const int LAST_COLOR = 37;

  static int color_number = RED;

  sprintf(kara, "\033[1;%dm 殻 " RESET, color_number);

  color_number++;
  if (color_number == LAST_COLOR + 1) {
    color_number = RED;
  }
}
