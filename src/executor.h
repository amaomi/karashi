/* executor.h - last stage of REPL, analyze ast and execute commands */

/* Copyright (c) 2022 Andrey Sikorin.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KARASHI_SRC_EXECUTOR_H_
#define KARASHI_SRC_EXECUTOR_H_

#include "parser.h"

// Execute commands from ast, handle I/O redirections and pipes
void execute(struct abstract_syntax_tree ast);

#endif //KARASHI_SRC_EXECUTOR_H_
